angular.module('app', [
  'ui.router',
  'ngResource',
  'ngSanitize',
  'truncate',
  //'lbServices',
  //'truncate',
  'ngCookies'
])

.config(['$urlRouterProvider', '$locationProvider', '$httpProvider', '$stateProvider',
  function($urlRouterProvider, $locationProvider, $httpProvider, $stateProvider) {

    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    //$urlRouterProvider.otherwise('/home');

    $stateProvider
      .state('home', {
        url: '/',
        controller: 'homeCtrl',
        controllerAs: 'home',
        templateUrl: '/app/home/index.html'
      });

    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('#');
    // {
    //   enabled: true,
    //   requireBase: false,
    //   rewriteLinks: false
    // }



  }
])

.controller('homeCtrl', function($scope, $rootScope, $state) {
  $scope.enrollmentOptions = {
    keyReading: 'You receive the same teaching and lectures as the certificate level, and are expected to read key articles a week and complete personal response sheets during the semester to help you process the material. No certificate for completion of Perspectives is given at this level.',
    certificate: 'Enhance your understanding of God\'s global purpose through lectures, activities, and selected readings. If you are not interested in receiving credit, we recommend this as the best option for getting the most out of Perspectives.',
    credit: 'Perspectives partners with accredited universities to offer undergraduate or graduate level credit hours for completing the course. These may be local or national credit options. Additional reading or assignments are sometimes required based on the credit granting institution'
  }


})

.run(function($rootScope, $state, $location, $window, Settings, anchorSmoothScroll) {
  $rootScope.settings = Settings;

  var wow = new WOW({
    boxClass: 'wow', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0, // distance to the element when triggering the animation (default is 0)
    mobile: false, // trigger animations on mobile devices (default is true)
    live: false // act on asynchronously loaded content (default is true)
  });

  wow.init();

  $rootScope.$on('$routeChangeStart', function(next, current) {
    //when the view changes sync wow
    wow.sync();
  });

  angular.element($window).bind("scroll", function() {
    var scroll = angular.element(window).scrollTop();
    if (angular.element(window).scrollTop()) {
      angular.element(".header-hide").addClass("scroll-header");
    } else {
      angular.element(".header-hide").removeClass("scroll-header");
    }
  });

  console.log('location hash:', $location.hash())




  // $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
  //   if ($location.hash()) $anchorScroll();
  // });

  // $rootScope.goto = function(elem) {
  //   // set the location.hash to the id of
  //   // the element you wish to scroll to.
  //   $location.hash(elem);

  //   // call $anchorScroll()
  //   anchorSmoothScroll.scrollTo(elem);
  // };
})
