Promise = require('bluebird');
var gulp = require('gulp');
var rimraf = require('gulp-rimraf');
var merge = require('merge-stream');
var del = require('del');
$ = require('gulp-load-plugins')(); // Note the extra parens

var deployDir = 'dist';

gulp.task('default', ['finish']);

gulp.task('clean', function(cb) {
    return gulp.src([deployDir + '/*', '!'+deployDir+'/.hg'], {
        read: false
    }).pipe(rimraf({
        force: true
    }));
});

gulp.task('copy', ['clean'], function() {
    return gulp.src(['assets/img/*', 'assets/img/**/*','assets/fonts/*', 'assets/content/*', 'app/**/*', 'app/*'])
        .pipe($.copy(deployDir + '/'));
});

gulp.task('bootstrap-fonts', ['copy'], function() {
    return gulp.src('assets/bower/bootstrap/fonts/*')
        .pipe(gulp.dest(deployDir + '/assets/fonts'));
});

gulp.task('fontawesome', ['copy'], function() {
    return gulp.src('assets/bower/font-awesome/fonts/*')
        .pipe(gulp.dest(deployDir + '/assets/fonts'));
});

gulp.task('fonts', ['bootstrap-fonts', 'fontawesome'])

var ngTemplates = function() {
    return gulp.src('app/**/*.html')
        .pipe($.angularTemplatecache({
            module: "app",
            root: '/app/'
        }))
        .pipe(gulp.dest('app'));
};

gulp.task('ng-templates', ['fonts'], ngTemplates);

gulp.task('templates', ngTemplates);

gulp.task('minify', ['ng-templates'], function() {
    return gulp.src('index.html')
        .pipe($.usemin({
            css: [$.minifyCss(), 'concat'],
            js: [$.ngAnnotate(), $.uglify(), 'concat']
        }))
        .pipe($.rev())
        .pipe(gulp.dest(deployDir + '/'));
});

// gulp.task('replace', ['minify'], function() {
//     var js = gulp.src(deployDir + '/js/*')
//         .pipe($.replace('assets/bower/angular-pretty-checkable/images/', '/img/'))
//         .pipe(gulp.dest(deployDir + '/js'));
//     return js;
// });

gulp.task('rename:index', ['minify'], function() {
    return gulp.src(deployDir + '/index-*.html')
        .pipe($.rename('index.html'))
        .pipe(gulp.dest(deployDir + "/"));
});

gulp.task('remove:index', ['rename:index'], function() {
    return gulp.src(deployDir + '/index-*.html').pipe($.rm())
})

// gulp.task('misc', ['remove:index'], function() {
//     var img = gulp.src(['assets/bower/angular-pretty-checkable/images/*'])
//         .pipe(gulp.dest(deployDir + '/images'));

//     return merge(img);
// })


gulp.task('finish', ['remove:index']);

gulp.task('watch', function() {
    var watchFiles = [
        'app/**/*.html'
    ];

    gulp.watch(watchFiles, ['templates']);
});
